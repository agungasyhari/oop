<?php

require ('animal.php');
require ('frog.php');
require ('ape.php');

$sheep = new animal('shaun', 2, "false");

echo "Nama : " . $sheep->name . "<br>"; // "shaun"
echo "Jumlah Kaki : " . $sheep->legs . "<br>"; // 2
echo "Berdarah Dingin : " . $sheep->cold_blooded . "<br>"; // false

echo "<br>";

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

$kodok = new frog("buduk");
echo "Nama : " . $kodok->name . "<br>"; // "shaun"
echo "Jumlah Kaki : " . $kodok->legs . "<br>"; // 2
echo "Berdarah Dingin : " . $kodok->cold_blooded . "<br>"; // false
echo "Suara : " . $kodok->jump() . "<br>";

echo "<br>" ;

$sungokong = new ape("kera sakti");
echo "Nama : " . $sungokong->name . "<br>"; // "shaun"
echo "Jumlah Kaki : " . $sungokong->legs . "<br>"; // 2
echo "Berdarah Dingin : " . $sungokong->cold_blooded . "<br>"; // false
echo "Suara : " . $sungokong->yell(); // "Auooo"


// $kodok = new Frog("buduk");
// $kodok->jump() ; // "hop hop"

?>  